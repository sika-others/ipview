from django.conf.urls.defaults import patterns, include, url

from views import  camera_view


urlpatterns = patterns('',
    url(r'^$',
        camera_view,
        name="ipview.camera", ),
)
