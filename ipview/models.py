# coding=utf8

import datetime

from django.db import models
from django.contrib.auth.models import User
from django.template.defaultfilters import slugify


ROLES = [
    ("owner", "Provozovatel"),
    ("worker", "Pracovník"),
]

class Account(models.Model):
    id_key = models.SlugField(unique=True, blank=True)
    name = models.CharField(max_length=64)

    records_url = models.CharField(max_length=255, blank=True, null=True)

    def __unicode__(self):
        return u"%s" % (self.name)

    def save(self, *args, **kwargs):
        self.id_key = slugify(self.name)
        super(Account, self).save(*args, **kwargs)

class UserProfile(User):
    account = models.ForeignKey(Account)

    role = models.CharField(max_length=16, choices=ROLES)

    def __unicode__(self):
        return u"%s (%s)" % (self.username, self.account.name)


class Camera(models.Model):
    account = models.ForeignKey(Account)

    name = models.CharField("Název", max_length=64)
    base_url = models.CharField(max_length=255)
    admin_url = models.CharField(max_length=255, blank=True, null=True)

    proxy_name = models.CharField(max_length=64, null=True, blank=True)
    proxy_pass = models.CharField(max_length=64, null=True, blank=True)

    img_url = models.CharField(max_length=255, blank=True, null=True)
    video_url = models.CharField(max_length=255, blank=True, null=True)

    def get_proxy(self):
        if self.proxy_name:
            return "%s:%s@" % (self.proxy_name, self.proxy_pass)
        return ""
    get_img_url = lambda self: self.get_proxy() + self.base_url + self.img_url
    get_video_url = lambda self: self.get_proxy() + self.base_url + self.video_url
    get_admin_url = lambda self: self.get_proxy() + self.base_url + self.admin_url

    def __unicode__(self):
        return u"%s (%s)" % (self.name, self.account.name)
