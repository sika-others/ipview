from django.shortcuts import render_to_response, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect
from django.template import RequestContext
from django.core.urlresolvers import reverse

from django.contrib.auth.decorators import login_required

from models import *

@login_required(login_url='/accounts/login/')
def camera_view(request, template="ipview/camera.html"):
    camera_list = Camera.objects.filter(account=request.user.userprofile.account)
    return render_to_response(template,
                              {
                                  "camera_img_list": camera_list.exclude(img_url=""),
                                  "camera_video_list": camera_list.exclude(video_url=""),
                                  "request": request,
                              },
                              context_instance=RequestContext(request))